/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hcapture_session.h"

#include "camera_util.h"
#include "camera_log.h"
#include "iconsumer_surface.h"
#include "ipc_skeleton.h"
#include "metadata_utils.h"
#include "bundle_mgr_interface.h"
#include "iservice_registry.h"
#include "system_ability_definition.h"

using namespace OHOS::AppExecFwk;
using namespace OHOS::AAFwk;

namespace OHOS {
namespace CameraStandard {
static std::map<int32_t, wptr<HCaptureSession>> session_;
static std::mutex sessionLock_;

static std::string GetClientBundle(int uid)
{
    std::string bundleName = "";
    auto samgr = SystemAbilityManagerClient::GetInstance().GetSystemAbilityManager();
    if (samgr == nullptr) {
        MEDIA_ERR_LOG("Get ability manager failed");
        return bundleName;
    }

    sptr<IRemoteObject> object = samgr->GetSystemAbility(BUNDLE_MGR_SERVICE_SYS_ABILITY_ID);
    if (object == nullptr) {
        MEDIA_DEBUG_LOG("object is NULL.");
        return bundleName;
    }

    sptr<AppExecFwk::IBundleMgr> bms = iface_cast<AppExecFwk::IBundleMgr>(object);
    if (bms == nullptr) {
        MEDIA_DEBUG_LOG("bundle manager service is NULL.");
        return bundleName;
    }

    auto result = bms->GetNameForUid(uid, bundleName);
    if (result != ERR_OK) {
        MEDIA_ERR_LOG("GetBundleNameForUid fail");
        return "";
    }
    MEDIA_INFO_LOG("bundle name is %{public}s ", bundleName.c_str());

    return bundleName;
}

HCaptureSession::HCaptureSession(sptr<HCameraHostManager> cameraHostManager,
    sptr<StreamOperatorCallback> streamOperatorCb, const uint32_t callingTokenId)
    : cameraHostManager_(cameraHostManager), streamOperatorCallback_(streamOperatorCb),
    sessionCallback_(nullptr)
{
    std::map<int32_t, wptr<HCaptureSession>>::iterator it;
    pid_ = IPCSkeleton::GetCallingPid();
    uid_ = IPCSkeleton::GetCallingUid();
    sessionState_.insert(std::make_pair(CaptureSessionState::SESSION_INIT, "Init"));
    sessionState_.insert(std::make_pair(CaptureSessionState::SESSION_CONFIG_INPROGRESS, "Config_In-progress"));
    sessionState_.insert(std::make_pair(CaptureSessionState::SESSION_CONFIG_COMMITTED, "Committed"));

    MEDIA_DEBUG_LOG("HCaptureSession: camera stub services(%{public}zu) pid(%{public}d).", session_.size(), pid_);
    std::map<int32_t, wptr<HCaptureSession>> oldSessions;
    for (auto it = session_.begin(); it != session_.end(); it++) {
        wptr<HCaptureSession> session = it->second;
        oldSessions[it->first] = session;
    }
    for (auto it = oldSessions.begin(); it != oldSessions.end(); it++) {
        wptr<HCaptureSession> session = it->second;
        wptr<HCameraDevice> disconnectDevice;
        int32_t rc = session->GetCameraDevice(disconnectDevice);
        if (rc == CAMERA_OK) {
            disconnectDevice->OnError(DEVICE_PREEMPT, 0);
        }
        session->Release(it->first);
        MEDIA_ERR_LOG("currently multi-session not supported, release session for pid(%{public}d)", it->first);
    }
    it = session_.find(pid_);
    if (it != session_.end()) {
        MEDIA_ERR_LOG("HCaptureSession::HCaptureSession doesn't support multiple sessions per pid");
    } else {
        session_[pid_] = this;
    }
    callerToken_ = callingTokenId;
    if (IsValidTokenId(callerToken_)) {
        StartUsingPermissionCallback(callerToken_, ACCESS_CAMERA);
        RegisterPermissionCallback(callerToken_, ACCESS_CAMERA);
    }
    MEDIA_DEBUG_LOG("HCaptureSession: camera stub services(%{public}zu).", session_.size());
}

HCaptureSession::~HCaptureSession()
{}

int32_t HCaptureSession::BeginConfig()
{
    CAMERA_SYNC_TRACE;
    if (curState_ == CaptureSessionState::SESSION_CONFIG_INPROGRESS) {
        MEDIA_ERR_LOG("HCaptureSession::BeginConfig Already in config inprogress state!");
        return CAMERA_INVALID_STATE;
    }
    std::lock_guard<std::mutex> lock(sessionLock_);
    prevState_ = curState_;
    curState_ = CaptureSessionState::SESSION_CONFIG_INPROGRESS;
    tempCameraDevices_.clear();
    tempStreams_.clear();
    deletedStreamIds_.clear();
    return CAMERA_OK;
}

int32_t HCaptureSession::AddInput(sptr<ICameraDeviceService> cameraDevice)
{
    CAMERA_SYNC_TRACE;
    sptr<HCameraDevice> localCameraDevice = nullptr;

    if (cameraDevice == nullptr) {
        MEDIA_ERR_LOG("HCaptureSession::AddInput cameraDevice is null");
        return CAMERA_INVALID_ARG;
    }
    if (curState_ != CaptureSessionState::SESSION_CONFIG_INPROGRESS) {
        MEDIA_ERR_LOG("HCaptureSession::AddInput Need to call BeginConfig before adding input");
        return CAMERA_INVALID_STATE;
    }
    auto item = cameraDevice_.promote();
    if (!tempCameraDevices_.empty() || (item != nullptr && !item->IsReleaseCameraDevice())) {
        MEDIA_ERR_LOG("HCaptureSession::AddInput Only one input is supported");
        return CAMERA_INVALID_SESSION_CFG;
    }
    localCameraDevice = static_cast<HCameraDevice*>(cameraDevice.GetRefPtr());
    if (item == localCameraDevice && item != nullptr) {
        item->SetReleaseCameraDevice(false);
    } else {
        tempCameraDevices_.emplace_back(localCameraDevice);
        CAMERA_SYSEVENT_STATISTIC(CreateMsg("CaptureSession::AddInput"));
    }

    sptr<IStreamOperator> streamOperator;
    int32_t rc = localCameraDevice->GetStreamOperator(streamOperatorCallback_, streamOperator);
    if (rc != CAMERA_OK) {
        MEDIA_ERR_LOG("HCaptureSession::GetCameraDevice GetStreamOperator returned %{public}d", rc);
        localCameraDevice->Close();
        return rc;
    }
    return CAMERA_OK;
}

int32_t HCaptureSession::AddOutputStream(sptr<HStreamCommon> stream)
{
    std::lock_guard<std::mutex> lock(sessionLock_);
    if (stream == nullptr) {
        MEDIA_ERR_LOG("HCaptureSession::AddOutputStream stream is null");
        return CAMERA_INVALID_ARG;
    }
    if (curState_ != CaptureSessionState::SESSION_CONFIG_INPROGRESS) {
        MEDIA_ERR_LOG("HCaptureSession::AddOutputStream Need to call BeginConfig before adding output");
        return CAMERA_INVALID_STATE;
    }
    if (std::find(tempStreams_.begin(), tempStreams_.end(), stream) != tempStreams_.end()) {
        MEDIA_ERR_LOG("HCaptureSession::AddOutputStream Adding same output multiple times in tempStreams_");
        return CAMERA_INVALID_SESSION_CFG;
    }
    auto it = std::find(streams_.begin(), streams_.end(), stream);
    if (it != streams_.end()) {
        if (stream && stream->IsReleaseStream()) {
            stream->SetReleaseStream(false);
            auto it2 = std::find(deletedStreamIds_.begin(), deletedStreamIds_.end(), stream->GetStreamId());
            if (it2 != deletedStreamIds_.end()) {
                deletedStreamIds_.erase(it2);
            }
            return CAMERA_OK;
        } else {
            MEDIA_ERR_LOG("HCaptureSession::AddOutputStream Adding same output multiple times in streams_");
            return CAMERA_INVALID_SESSION_CFG;
        }
    }
    if (stream) {
        stream->SetReleaseStream(false);
    }
    tempStreams_.emplace_back(stream);
    return CAMERA_OK;
}

int32_t HCaptureSession::AddOutput(StreamType streamType, sptr<IStreamCommon> stream)
{
    int32_t rc = CAMERA_INVALID_ARG;
    if (stream == nullptr) {
        MEDIA_ERR_LOG("HCaptureSession::AddOutput stream is null");
        return rc;
    }
    // Temp hack to fix the library linking issue
    sptr<IConsumerSurface> captureSurface = IConsumerSurface::Create();

    if (streamType == StreamType::CAPTURE) {
        rc = AddOutputStream(static_cast<HStreamCapture *>(stream.GetRefPtr()));
    } else if (streamType == StreamType::REPEAT) {
        rc = AddOutputStream(static_cast<HStreamRepeat *>(stream.GetRefPtr()));
    }  else if (streamType == StreamType::METADATA) {
        rc = AddOutputStream(static_cast<HStreamMetadata *>(stream.GetRefPtr()));
    }
    CAMERA_SYSEVENT_STATISTIC(CreateMsg("CaptureSession::AddOutput with %d", streamType));
    MEDIA_INFO_LOG("CaptureSession::AddOutput with with %{public}d, rc = %{public}d", streamType, rc);
    return rc;
}

int32_t HCaptureSession::RemoveInput(sptr<ICameraDeviceService> cameraDevice)
{
    if (cameraDevice == nullptr) {
        MEDIA_ERR_LOG("HCaptureSession::RemoveInput cameraDevice is null");
        return CAMERA_INVALID_ARG;
    }
    if (curState_ != CaptureSessionState::SESSION_CONFIG_INPROGRESS) {
        MEDIA_ERR_LOG("HCaptureSession::RemoveInput Need to call BeginConfig before removing input");
        return CAMERA_INVALID_STATE;
    }
    std::lock_guard<std::mutex> lock(sessionLock_);
    wptr<HCameraDevice> localCameraDevice;
    localCameraDevice = static_cast<HCameraDevice*>(cameraDevice.GetRefPtr());
    auto item = cameraDevice_.promote();
    auto it = std::find(tempCameraDevices_.begin(), tempCameraDevices_.end(), localCameraDevice);
    if (it != tempCameraDevices_.end()) {
        tempCameraDevices_.erase(it);
    } else if (item != nullptr && cameraDevice_ == localCameraDevice) {
        item->SetReleaseCameraDevice(true);
    } else {
        MEDIA_ERR_LOG("HCaptureSession::RemoveInput Invalid camera device");
        return CAMERA_INVALID_SESSION_CFG;
    }
    CAMERA_SYSEVENT_STATISTIC(CreateMsg("CaptureSession::RemoveInput"));
    return CAMERA_OK;
}

int32_t HCaptureSession::RemoveOutputStream(sptr<HStreamCommon> stream)
{
    if (stream == nullptr) {
        MEDIA_ERR_LOG("HCaptureSession::RemoveOutputStream stream is null");
        return CAMERA_INVALID_ARG;
    }
    if (curState_ != CaptureSessionState::SESSION_CONFIG_INPROGRESS) {
        MEDIA_ERR_LOG("HCaptureSession::RemoveOutputStream Need to call BeginConfig before removing output");
        return CAMERA_INVALID_STATE;
    }
    auto it = std::find(tempStreams_.begin(), tempStreams_.end(), stream);
    if (it != tempStreams_.end()) {
        tempStreams_.erase(it);
    } else {
        it = std::find(streams_.begin(), streams_.end(), stream);
        if (it != streams_.end()) {
            if (stream && !stream->IsReleaseStream()) {
                deletedStreamIds_.emplace_back(stream->GetStreamId());
                stream->SetReleaseStream(true);
            }
        } else {
            MEDIA_ERR_LOG("HCaptureSession::RemoveOutputStream Invalid output");
            return CAMERA_INVALID_SESSION_CFG;
        }
    }
    return CAMERA_OK;
}

int32_t HCaptureSession::RemoveOutput(StreamType streamType, sptr<IStreamCommon> stream)
{
    int32_t rc = CAMERA_INVALID_ARG;
    if (stream == nullptr) {
        MEDIA_ERR_LOG("HCaptureSession::RemoveOutput stream is null");
        return rc;
    }
    std::lock_guard<std::mutex> lock(sessionLock_);
    if (streamType == StreamType::CAPTURE) {
        rc = RemoveOutputStream(static_cast<HStreamCapture *>(stream.GetRefPtr()));
    } else if (streamType == StreamType::REPEAT) {
        rc = RemoveOutputStream(static_cast<HStreamRepeat *>(stream.GetRefPtr()));
    } else if (streamType == StreamType::METADATA) {
        rc = RemoveOutputStream(static_cast<HStreamMetadata *>(stream.GetRefPtr()));
    }
    CAMERA_SYSEVENT_STATISTIC(CreateMsg("CaptureSession::RemoveOutput with %d", streamType));
    return rc;
}

int32_t HCaptureSession::ValidateSessionInputs()
{
    auto item = cameraDevice_.promote();
    if (tempCameraDevices_.empty() && (item == nullptr || item->IsReleaseCameraDevice())) {
        MEDIA_ERR_LOG("HCaptureSession::ValidateSessionInputs No inputs present");
        return CAMERA_INVALID_SESSION_CFG;
    }
    return CAMERA_OK;
}

int32_t HCaptureSession::ValidateSessionOutputs()
{
    if (tempStreams_.size() + streams_.size() - deletedStreamIds_.size() == 0) {
        MEDIA_ERR_LOG("HCaptureSession::ValidateSessionOutputs No outputs present");
        return CAMERA_INVALID_SESSION_CFG;
    }
    return CAMERA_OK;
}

int32_t HCaptureSession::GetCameraDevice(wptr<HCameraDevice> &device)
{
    auto item = cameraDevice_.promote();
    if (item != nullptr && !item->IsReleaseCameraDevice()) {
        MEDIA_DEBUG_LOG("HCaptureSession::GetCameraDevice Camera device has not changed");
        device = cameraDevice_;
        return CAMERA_OK;
    } else if (!tempCameraDevices_.empty()) {
        device = tempCameraDevices_[0];
        return CAMERA_OK;
    }

    MEDIA_ERR_LOG("HCaptureSession::GetCameraDevice Failed because don't have camera device");
    return CAMERA_INVALID_STATE;
}

int32_t HCaptureSession::GetCurrentStreamInfos(wptr<HCameraDevice> &device,
                                               std::shared_ptr<OHOS::Camera::CameraMetadata> &deviceSettings,
                                               std::vector<StreamInfo> &streamInfos)
{
    int32_t rc;
    int32_t streamId = streamId_;
    bool isNeedLink;
    StreamInfo curStreamInfo;
    sptr<IStreamOperator> streamOperator;
    sptr<HStreamCommon> curStream;

    auto item = device.promote();
    if (item != nullptr) {
        streamOperator = item->GetStreamOperator();
    }
    isNeedLink = (device != cameraDevice_);
    for (auto item = streams_.begin(); item != streams_.end(); ++item) {
        curStream = *item;
        if (curStream && curStream->IsReleaseStream()) {
            continue;
        }
        if (curStream && isNeedLink) {
            rc = curStream->LinkInput(streamOperator, deviceSettings, streamId);
            if (rc != CAMERA_OK) {
                MEDIA_ERR_LOG("HCaptureSession::GetCurrentStreamInfos() Failed to link Output, %{public}d", rc);
                return rc;
            }
            streamId++;
        }
        if (curStream) {
            curStream->SetStreamInfo(curStreamInfo);
        }
        streamInfos.push_back(curStreamInfo);
    }

    if (streamId != streamId_) {
        streamId_ = streamId;
    }
    return CAMERA_OK;
}

int32_t HCaptureSession::CreateAndCommitStreams(wptr<HCameraDevice> &device,
                                                std::shared_ptr<OHOS::Camera::CameraMetadata> &deviceSettings,
                                                std::vector<StreamInfo> &streamInfos)
{
    CamRetCode hdiRc = HDI::Camera::V1_0::NO_ERROR;
    StreamInfo curStreamInfo;
    sptr<IStreamOperator> streamOperator;
    auto itemDevice = device.promote();
    if (itemDevice != nullptr) {
        streamOperator = itemDevice->GetStreamOperator();
    }
    if (streamOperator != nullptr && !streamInfos.empty()) {
        hdiRc = (CamRetCode)(streamOperator->CreateStreams(streamInfos));
    } else {
        MEDIA_INFO_LOG("HCaptureSession::CreateAndCommitStreams(), No new streams to create");
    }
    if (streamOperator != nullptr && hdiRc == HDI::Camera::V1_0::NO_ERROR) {
        std::vector<uint8_t> setting;
        OHOS::Camera::MetadataUtils::ConvertMetadataToVec(deviceSettings, setting);
        hdiRc = (CamRetCode)(streamOperator->CommitStreams(NORMAL, setting));
        if (hdiRc != HDI::Camera::V1_0::NO_ERROR) {
            MEDIA_ERR_LOG("HCaptureSession::CreateAndCommitStreams(), Failed to commit %{public}d", hdiRc);
            std::vector<int32_t> streamIds;
            for (auto item = streamInfos.begin(); item != streamInfos.end(); ++item) {
                curStreamInfo = *item;
                streamIds.emplace_back(curStreamInfo.streamId_);
            }
            if (!streamIds.empty() && streamOperator->ReleaseStreams(streamIds) != HDI::Camera::V1_0::NO_ERROR) {
                MEDIA_ERR_LOG("HCaptureSession::CreateAndCommitStreams(), Failed to release streams");
            }
        }
    }
    return HdiToServiceError(hdiRc);
}

int32_t HCaptureSession::CheckAndCommitStreams(wptr<HCameraDevice> &device,
                                               std::shared_ptr<OHOS::Camera::CameraMetadata> &deviceSettings,
                                               std::vector<StreamInfo> &allStreamInfos,
                                               std::vector<StreamInfo> &newStreamInfos)
{
    return CreateAndCommitStreams(device, deviceSettings, newStreamInfos);
}

void HCaptureSession::DeleteReleasedStream()
{
    auto matchFunction = [](const auto& curStream) { return curStream->IsReleaseStream();};
    captureStreams_.erase(std::remove_if(captureStreams_.begin(), captureStreams_.end(), matchFunction),
        captureStreams_.end());
    repeatStreams_.erase(std::remove_if(repeatStreams_.begin(), repeatStreams_.end(), matchFunction),
        repeatStreams_.end());
    metadataStreams_.erase(std::remove_if(metadataStreams_.begin(), metadataStreams_.end(), matchFunction),
        metadataStreams_.end());
    sptr<HStreamCommon> curStream;
    for (auto item = streams_.begin(); item != streams_.end(); ++item) {
        curStream = *item;
        if (curStream && curStream->IsReleaseStream()) {
            curStream->Release();
            streams_.erase(item--);
        }
    }
}

void HCaptureSession::RestorePreviousState(wptr<HCameraDevice> &device, bool isCreateReleaseStreams)
{
    std::vector<StreamInfo> streamInfos;
    StreamInfo streamInfo;
    std::shared_ptr<OHOS::Camera::CameraMetadata> settings;
    sptr<HStreamCommon> curStream;

    MEDIA_DEBUG_LOG("HCaptureSession::RestorePreviousState, Restore to previous state");

    for (auto item = streams_.begin(); item != streams_.end(); ++item) {
        curStream = *item;
        if (curStream && isCreateReleaseStreams && curStream->IsReleaseStream()) {
            curStream->SetStreamInfo(streamInfo);
            streamInfos.push_back(streamInfo);
        }
        if (curStream) {
            curStream->SetReleaseStream(false);
        }
    }

    for (auto item = tempStreams_.begin(); item != tempStreams_.end(); ++item) {
        curStream = *item;
        if (curStream) {
            curStream->Release();
        }
    }
    tempStreams_.clear();
    deletedStreamIds_.clear();
    tempCameraDevices_.clear();
    auto item = device.promote();
    if (item != nullptr) {
        item->SetReleaseCameraDevice(false);
        if (isCreateReleaseStreams) {
            settings = item->GetSettings();
            if (settings != nullptr) {
                CreateAndCommitStreams(device, settings, streamInfos);
            }
        }
    }
    curState_ = prevState_;
}

void HCaptureSession::UpdateSessionConfig(wptr<HCameraDevice> &device)
{
    DeleteReleasedStream();
    deletedStreamIds_.clear();

    sptr<HStreamCommon> curStream;
    for (auto item = tempStreams_.begin(); item != tempStreams_.end(); ++item) {
        curStream = *item;
        if (curStream && curStream->GetStreamType() == StreamType::REPEAT) {
            repeatStreams_.emplace_back(curStream);
        } else if (curStream && curStream->GetStreamType() == StreamType::CAPTURE) {
            captureStreams_.emplace_back(curStream);
        } else if (curStream && curStream->GetStreamType() == StreamType::METADATA) {
            metadataStreams_.emplace_back(curStream);
        }
        streams_.emplace_back(curStream);
    }
    tempStreams_.clear();
    if (streamOperatorCallback_ == nullptr) {
        MEDIA_ERR_LOG("HCaptureSession::UpdateSessionConfig streamOperatorCallback_ is nullptr");
        return;
    }
    streamOperatorCallback_->SetCaptureSession(this);
    cameraDevice_ = device;
    curState_ = CaptureSessionState::SESSION_CONFIG_COMMITTED;
}

int32_t HCaptureSession::HandleCaptureOuputsConfig(wptr<HCameraDevice> &device)
{
    int32_t rc;
    int32_t streamId;
    std::vector<StreamInfo> newStreamInfos;
    std::vector<StreamInfo> allStreamInfos;
    StreamInfo curStreamInfo;
    std::shared_ptr<OHOS::Camera::CameraMetadata> settings;
    sptr<IStreamOperator> streamOperator;
    sptr<HStreamCommon> curStream;

    auto item = device.promote();
    if (item != nullptr) {
        settings = item->GetSettings();
    }
    if (item == nullptr || settings == nullptr) {
        return CAMERA_UNKNOWN_ERROR;
    }

    rc = GetCurrentStreamInfos(device, settings, allStreamInfos);
    if (rc != CAMERA_OK) {
        MEDIA_ERR_LOG("HCaptureSession::HandleCaptureOuputsConfig() Failed to get streams info, %{public}d", rc);
        return rc;
    }

    if (cameraDevice_ != device) {
        newStreamInfos = allStreamInfos;
    }

    streamOperator = device->GetStreamOperator();
    streamId = streamId_;
    for (auto item = tempStreams_.begin(); item != tempStreams_.end(); ++item) {
        curStream = *item;
        if (curStream == nullptr) {
            MEDIA_ERR_LOG("HCaptureSession::HandleCaptureOuputsConfig() curStream is null");
            return CAMERA_UNKNOWN_ERROR;
        }
        if (curStream) {
            rc = curStream->LinkInput(streamOperator, settings, streamId);
            if (rc != CAMERA_OK) {
                MEDIA_ERR_LOG("HCaptureSession::HandleCaptureOuputsConfig() Failed to link Output, %{public}d", rc);
                return rc;
            }
        }
        if (curStream) {
            curStream->SetStreamInfo(curStreamInfo);
        }
        newStreamInfos.push_back(curStreamInfo);
        allStreamInfos.push_back(curStreamInfo);
        streamId++;
    }

    rc = CheckAndCommitStreams(device, settings, allStreamInfos, newStreamInfos);
    if (rc == CAMERA_OK) {
        streamId_ = streamId;
    }
    return rc;
}

int32_t HCaptureSession::CommitConfig()
{
    wptr<HCameraDevice> device = nullptr;

    if (curState_ != CaptureSessionState::SESSION_CONFIG_INPROGRESS) {
        MEDIA_ERR_LOG("HCaptureSession::CommitConfig() Need to call BeginConfig before committing configuration");
        return CAMERA_INVALID_STATE;
    }

    int32_t rc = ValidateSessionInputs();
    if (rc != CAMERA_OK) {
        return rc;
    }
    rc = ValidateSessionOutputs();
    if (rc != CAMERA_OK) {
        return rc;
    }

    std::lock_guard<std::mutex> lock(sessionLock_);
    rc = GetCameraDevice(device);
    auto item = device.promote();
    if ((rc == CAMERA_OK) && (device == cameraDevice_) && !deletedStreamIds_.empty()) {
        rc = HdiToServiceError((CamRetCode)(device->GetStreamOperator()->ReleaseStreams(deletedStreamIds_)));
    }

    if (rc != CAMERA_OK) {
        MEDIA_ERR_LOG("HCaptureSession::CommitConfig() Failed to commit config. camera device rc: %{public}d", rc);
        if (item != nullptr && device != cameraDevice_) {
            device->Close();
        }
        RestorePreviousState(cameraDevice_, false);
        return rc;
    }

    rc = HandleCaptureOuputsConfig(device);
    if (rc != CAMERA_OK) {
        MEDIA_ERR_LOG("HCaptureSession::CommitConfig() Failed to commit config. rc: %{public}d", rc);
        if (item != nullptr && device != cameraDevice_) {
            device->Close();
        }
        RestorePreviousState(cameraDevice_, !deletedStreamIds_.empty());
        return rc;
    }
    if (item != nullptr) {
        int32_t pid = IPCSkeleton::GetCallingPid();
        int32_t uid = IPCSkeleton::GetCallingUid();
        POWERMGR_SYSEVENT_CAMERA_CONNECT(pid, uid, device->GetCameraId().c_str(),
                                         GetClientBundle(uid));
    }

    if (cameraDevice_ != nullptr && device != cameraDevice_) {
        cameraDevice_->Close();
        cameraDevice_ = nullptr;
    }
    UpdateSessionConfig(device);
    return rc;
}

int32_t HCaptureSession::GetSessionState(CaptureSessionState &sessionState)
{
    int32_t rc = CAMERA_OK;
    sessionState = curState_;
    return rc;
}

int32_t HCaptureSession::Start()
{
    int32_t rc = CAMERA_OK;
    sptr<HStreamRepeat> curStreamRepeat;
    if (curState_ != CaptureSessionState::SESSION_CONFIG_COMMITTED) {
        MEDIA_ERR_LOG("HCaptureSession::Start(), Invalid session state: %{public}d", rc);
        return CAMERA_INVALID_STATE;
    }
    std::lock_guard<std::mutex> lock(sessionLock_);
    for (auto item = repeatStreams_.begin(); item != repeatStreams_.end(); ++item) {
        curStreamRepeat = static_cast<HStreamRepeat *>((*item).GetRefPtr());
        if (!curStreamRepeat->IsVideo()) {
            rc = curStreamRepeat->Start();
            if (rc != CAMERA_OK) {
                MEDIA_ERR_LOG("HCaptureSession::Start(), Failed to start preview, rc: %{public}d", rc);
                break;
            }
        }
    }
    return rc;
}

int32_t HCaptureSession::Stop()
{
    int32_t rc = CAMERA_OK;
    sptr<HStreamRepeat> curStreamRepeat;
    if (curState_ != CaptureSessionState::SESSION_CONFIG_COMMITTED) {
        return CAMERA_INVALID_STATE;
    }
    std::lock_guard<std::mutex> lock(sessionLock_);
    for (auto item = repeatStreams_.begin(); item != repeatStreams_.end(); ++item) {
        curStreamRepeat = static_cast<HStreamRepeat *>((*item).GetRefPtr());
        if (!curStreamRepeat->IsVideo()) {
            rc = curStreamRepeat->Stop();
            if (rc != CAMERA_OK) {
                MEDIA_ERR_LOG("HCaptureSession::Stop(), Failed to stop preview, rc: %{public}d", rc);
                break;
            }
        }
    }

    return rc;
}

void HCaptureSession::ClearCaptureSession(pid_t pid)
{
    MEDIA_DEBUG_LOG("ClearCaptureSession: camera stub services(%{public}zu) pid(%{public}d).", session_.size(), pid);
    auto it = session_.find(pid);
    if (it != session_.end()) {
        session_.erase(it);
    }
    MEDIA_DEBUG_LOG("ClearCaptureSession: camera stub services(%{public}zu).", session_.size());
}

void HCaptureSession::ReleaseStreams()
{
    std::vector<int32_t> streamIds;
    sptr<HStreamCommon> curStream;

    for (auto item = streams_.begin(); item != streams_.end(); ++item) {
        curStream = *item;
        if (curStream) {
            streamIds.emplace_back(curStream->GetStreamId());
            curStream->Release();
        }
    }
    repeatStreams_.clear();
    captureStreams_.clear();
    metadataStreams_.clear();
    streams_.clear();
    auto item = cameraDevice_.promote();
    if ((item != nullptr) && (item->GetStreamOperator() != nullptr) && !streamIds.empty()) {
        item->GetStreamOperator()->ReleaseStreams(streamIds);
    }
}

int32_t HCaptureSession::ReleaseInner()
{
    MEDIA_INFO_LOG("HCaptureSession::ReleaseInner enter");
    return Release(pid_);
}

int32_t HCaptureSession::Release(pid_t pid)
{
    std::lock_guard<std::mutex> lock(sessionLock_);
    MEDIA_INFO_LOG("HCaptureSession::Release pid(%{public}d).", pid);
    auto it = session_.find(pid);
    if (it == session_.end()) {
        MEDIA_DEBUG_LOG("HCaptureSession::Release session for pid(%{public}d) already released.", pid);
        return CAMERA_OK;
    }
    ReleaseStreams();
    tempCameraDevices_.clear();
    if (streamOperatorCallback_ != nullptr) {
        streamOperatorCallback_->SetCaptureSession(nullptr);
        streamOperatorCallback_ = nullptr;
    }
    auto item = cameraDevice_.promote();
    if (item != nullptr) {
        item->Close();
        POWERMGR_SYSEVENT_CAMERA_DISCONNECT(item->GetCameraId().c_str());
        item = nullptr;
    }
    if (IsValidTokenId(callerToken_)) {
        StopUsingPermissionCallback(callerToken_, ACCESS_CAMERA);
        UnregisterPermissionCallback(callerToken_);
    }
    tempStreams_.clear();
    sessionState_.clear();
    ClearCaptureSession(pid);
    sessionCallback_ = nullptr;
    cameraHostManager_ = nullptr;
    return CAMERA_OK;
}

void HCaptureSession::RegisterPermissionCallback(const uint32_t callingTokenId, const std::string permissionName)
{
    Security::AccessToken::PermStateChangeScope scopeInfo;
    scopeInfo.permList = {permissionName};
    scopeInfo.tokenIDs = {callingTokenId};
    callbackPtr_ = std::make_shared<PermissionStatusChangeCb>(scopeInfo);
    callbackPtr_->SetCaptureSession(this);
    MEDIA_DEBUG_LOG("after tokenId:%{public}d register", callingTokenId);
    int32_t res = Security::AccessToken::AccessTokenKit::RegisterPermStateChangeCallback(callbackPtr_);
    if (res != CAMERA_OK) {
        MEDIA_ERR_LOG("RegisterPermStateChangeCallback failed.");
    }
}

void HCaptureSession::UnregisterPermissionCallback(const uint32_t callingTokenId)
{
    if (callbackPtr_ == nullptr) {
        MEDIA_ERR_LOG("callbackPtr_ is null.");
        return;
    }
    int32_t res = Security::AccessToken::AccessTokenKit::UnRegisterPermStateChangeCallback(callbackPtr_);
    if (res != CAMERA_OK) {
        MEDIA_ERR_LOG("UnRegisterPermStateChangeCallback failed.");
    }
    if (callbackPtr_) {
        callbackPtr_->SetCaptureSession(nullptr);
        callbackPtr_ = nullptr;
    }
    MEDIA_DEBUG_LOG("after tokenId:%{public}d unregister", callingTokenId);
}

void HCaptureSession::DestroyStubObjectForPid(pid_t pid)
{
    MEDIA_DEBUG_LOG("camera stub services(%{public}zu) pid(%{public}d).", session_.size(), pid);
    sptr<HCaptureSession> session;

    auto it = session_.find(pid);
    if (it != session_.end()) {
        session = it->second;
        session->Release(pid);
    }
    MEDIA_DEBUG_LOG("camera stub services(%{public}zu).", session_.size());
}

int32_t HCaptureSession::SetCallback(sptr<ICaptureSessionCallback> &callback)
{
    if (callback == nullptr) {
        MEDIA_ERR_LOG("HCaptureSession::SetCallback callback is null");
        return CAMERA_INVALID_ARG;
    }

    sessionCallback_ = callback;
    return CAMERA_OK;
}

std::string HCaptureSession::GetSessionState()
{
    std::map<CaptureSessionState, std::string>::const_iterator iter =
        sessionState_.find(curState_);
    if (iter != sessionState_.end()) {
        return iter->second;
    }
    return nullptr;
}

void HCaptureSession::CameraSessionSummary(std::string& dumpString)
{
    dumpString += "# Number of Camera clients:[" + std::to_string(session_.size()) + "]:\n";
}

void HCaptureSession::dumpSessions(std::string& dumpString)
{
    for (auto it = session_.begin(); it != session_.end(); it++) {
        wptr<HCaptureSession> session = it->second;
        dumpString += "No. of sessions for client:[" + std::to_string(1) + "]:\n";
        session->dumpSessionInfo(dumpString);
    }
}

void HCaptureSession::dumpSessionInfo(std::string& dumpString)
{
    dumpString += "Client pid:[" + std::to_string(pid_)
        + "]    Client uid:[" + std::to_string(uid_) + "]:\n";
    dumpString += "session state:[" + GetSessionState() + "]:\n";
    auto item = cameraDevice_.promote();
    if (item != nullptr) {
        dumpString += "session Camera Id:[" + cameraDevice_->GetCameraId() + "]:\n";
        dumpString += "session Camera release status:["
        + std::to_string(cameraDevice_->IsReleaseCameraDevice()) + "]:\n";
    }
    for (const auto& stream : captureStreams_) {
        stream->DumpStreamInfo(dumpString);
    }
    for (const auto& stream : repeatStreams_) {
        stream->DumpStreamInfo(dumpString);
    }
    for (const auto& stream : metadataStreams_) {
        stream->DumpStreamInfo(dumpString);
    }
}

void HCaptureSession::StartUsingPermissionCallback(const uint32_t callingTokenId, const std::string permissionName)
{
    cameraUseCallbackPtr_ = std::make_shared<CameraUseStateChangeCb>();
    cameraUseCallbackPtr_->SetCaptureSession(this);
    MEDIA_DEBUG_LOG("after StartUsingPermissionCallback tokenId:%{public}d", callingTokenId);
    int32_t res = Security::AccessToken::PrivacyKit::StartUsingPermission(
        callingTokenId, permissionName, cameraUseCallbackPtr_);
    if (res != CAMERA_OK) {
        MEDIA_ERR_LOG("StartUsingPermissionCallback failed.");
    }
}

void HCaptureSession::StopUsingPermissionCallback(const uint32_t callingTokenId, const std::string permissionName)
{
    MEDIA_DEBUG_LOG("enter StopUsingPermissionCallback tokenId:%{public}d", callingTokenId);
    int32_t res = Security::AccessToken::PrivacyKit::StopUsingPermission(callingTokenId, permissionName);
    if (res != CAMERA_OK) {
        MEDIA_ERR_LOG("StopUsingPermissionCallback failed.");
    }
    if (cameraUseCallbackPtr_) {
        cameraUseCallbackPtr_->SetCaptureSession(nullptr);
        cameraUseCallbackPtr_ = nullptr;
    }
}

PermissionStatusChangeCb::~PermissionStatusChangeCb()
{
    captureSession_ = nullptr;
}

void PermissionStatusChangeCb::SetCaptureSession(wptr<HCaptureSession> captureSession)
{
    captureSession_ = captureSession;
}

void PermissionStatusChangeCb::PermStateChangeCallback(Security::AccessToken::PermStateChangeInfo& result)
{
    if ((result.permStateChangeType == 0) && (captureSession_ != nullptr)) {
        captureSession_->ReleaseInner();
    }
};

CameraUseStateChangeCb::~CameraUseStateChangeCb()
{
    captureSession_ = nullptr;
}

void CameraUseStateChangeCb::SetCaptureSession(wptr<HCaptureSession> captureSession)
{
    captureSession_ = captureSession;
}

void CameraUseStateChangeCb::StateChangeNotify(Security::AccessToken::AccessTokenID tokenId, bool isShowing)
{
    const int32_t delayProcessTime = 200000;
    usleep(delayProcessTime);
    MEDIA_INFO_LOG("enter CameraUseStateChangeNotify tokenId:%{public}d", tokenId);
    if ((isShowing == false) && (captureSession_ != nullptr)) {
        captureSession_->ReleaseInner();
    }
};


StreamOperatorCallback::StreamOperatorCallback(sptr<HCaptureSession> session)
{
    captureSession_ = session;
}

StreamOperatorCallback::~StreamOperatorCallback()
{
    captureSession_ = nullptr;
}

sptr<HStreamCommon> StreamOperatorCallback::GetStreamByStreamID(int32_t streamId)
{
    sptr<HStreamCommon> curStream;
    sptr<HStreamCommon> result = nullptr;
    std::lock_guard<std::mutex> lock(sessionLock_);
    if (captureSession_ != nullptr) {
        for (auto item = captureSession_->streams_.begin(); item != captureSession_->streams_.end(); ++item) {
            curStream = *item;
            if (curStream && curStream->GetStreamId() == streamId) {
                result = curStream;
                break;
            }
        }
    }
    return result;
}

int32_t StreamOperatorCallback::OnCaptureStarted(int32_t captureId,
                                                 const std::vector<int32_t> &streamIds)
{
    sptr<HStreamCommon> curStream;

    for (auto item = streamIds.begin(); item != streamIds.end(); ++item) {
        curStream = GetStreamByStreamID(*item);
        if (curStream == nullptr) {
            MEDIA_ERR_LOG("StreamOperatorCallback::OnCaptureStarted StreamId: %{public}d not found", *item);
            return CAMERA_INVALID_ARG;
        } else if (curStream->GetStreamType() == StreamType::REPEAT) {
            static_cast<HStreamRepeat *>(curStream.GetRefPtr())->OnFrameStarted();
        } else if (curStream->GetStreamType() == StreamType::CAPTURE) {
            static_cast<HStreamCapture *>(curStream.GetRefPtr())->OnCaptureStarted(captureId);
        }
    }
    return CAMERA_OK;
}

int32_t StreamOperatorCallback::OnCaptureEnded(int32_t captureId, const std::vector<CaptureEndedInfo>& infos)
{
    sptr<HStreamCommon> curStream;
    CaptureEndedInfo captureInfo;

    for (auto item = infos.begin(); item != infos.end(); ++item) {
        captureInfo = *item;
        curStream = GetStreamByStreamID(captureInfo.streamId_);
        if (curStream == nullptr) {
            MEDIA_ERR_LOG("StreamOperatorCallback::OnCaptureEnded StreamId: %{public}d not found."
                          " Framecount: %{public}d", captureInfo.streamId_, captureInfo.frameCount_);
            return CAMERA_INVALID_ARG;
        } else if (curStream->GetStreamType() == StreamType::REPEAT) {
            static_cast<HStreamRepeat *>(curStream.GetRefPtr())->OnFrameEnded(captureInfo.frameCount_);
        } else if (curStream->GetStreamType() == StreamType::CAPTURE) {
            static_cast<HStreamCapture *>(curStream.GetRefPtr())->OnCaptureEnded(captureId, captureInfo.frameCount_);
        }
    }
    return CAMERA_OK;
}

int32_t StreamOperatorCallback::OnCaptureError(int32_t captureId, const std::vector<CaptureErrorInfo>& infos)
{
    sptr<HStreamCommon> curStream;
    CaptureErrorInfo errInfo;

    for (auto item = infos.begin(); item != infos.end(); ++item) {
        errInfo = *item;
        curStream = GetStreamByStreamID(errInfo.streamId_);
        if (curStream == nullptr) {
            MEDIA_ERR_LOG("StreamOperatorCallback::OnCaptureError StreamId: %{public}d not found."
                          " Error: %{public}d", errInfo.streamId_, errInfo.error_);
            return CAMERA_INVALID_ARG;
        } else if (curStream->GetStreamType() == StreamType::REPEAT) {
            static_cast<HStreamRepeat *>(curStream.GetRefPtr())->OnFrameError(errInfo.error_);
        } else if (curStream->GetStreamType() == StreamType::CAPTURE) {
            static_cast<HStreamCapture *>(curStream.GetRefPtr())->OnCaptureError(captureId, errInfo.error_);
        }
    }
    return CAMERA_OK;
}

int32_t StreamOperatorCallback::OnFrameShutter(int32_t captureId,
                                               const std::vector<int32_t> &streamIds,
                                               uint64_t timestamp)
{
    sptr<HStreamCommon> curStream;

    for (auto item = streamIds.begin(); item != streamIds.end(); ++item) {
        curStream = GetStreamByStreamID(*item);
        if ((curStream != nullptr) && (curStream->GetStreamType() == StreamType::CAPTURE)) {
            static_cast<HStreamCapture *>(curStream.GetRefPtr())->OnFrameShutter(captureId, timestamp);
        } else {
            MEDIA_ERR_LOG("StreamOperatorCallback::OnFrameShutter StreamId: %{public}d not found", *item);
            return CAMERA_INVALID_ARG;
        }
    }
    return CAMERA_OK;
}

void StreamOperatorCallback::SetCaptureSession(wptr<HCaptureSession> captureSession)
{
    captureSession_ = captureSession;
}
} // namespace CameraStandard
} // namespace OHOS
